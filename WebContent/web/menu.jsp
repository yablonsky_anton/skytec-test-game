<%
    long start = (long) request.getAttribute("PageStartTime");
    HttpSession session = request.getSession(false);
    long dbTime = (long) session.getAttribute("DBMenuTime");
    int dbReqs = (int) session.getAttribute("DBMenuReqs");
%>
<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<style>
    .top-outer-div {
        position: absolute;
        width: 100%
    }

    .bottom-outer-div {
        position: absolute;
        bottom: 10px;
        width: 100%
    }

    .inner-div {
        background-color: rgba(242, 242, 242, .3);
        width: 50%;
        margin: 10px auto auto auto;
        padding: 20px
    }

    * {
        font-family: Helvetica, serif;
        border-radius: 5px;
        box-sizing: border-box
    }

    .form-container {
        float: left;
        width: 50%;
        padding: 5px
    }

    .inner-div::after {
        content: "";
        clear: both;
        display: table
    }

    body {
        background-image: url(../images/background.svg);
        background-color: #ccc;
        background-repeat: no-repeat;
        background-size: cover;
    }

    input[type=submit] {
        width: 100%;
        color: #fff;
        padding: 14px 20px;
        margin: 8px 0;
        border: 0;
        border-radius: 4px;
        cursor: pointer;
        font-size: 10pt
    }

    .duel {
        background-color: #af9242
    }

    .logout {
        background-color: #b23f1b
    }

    .duel:hover {
        background-color: #806340
    }

    .logout:hover {
        background-color: #802b17
    }
</style>


<head>
    <meta charset="UTF-8">
    <title>Main Page</title>
</head>

<body>
<div class="top-outer-div">
    <div class="inner-div">
        <h3>Играть или выйти?</h3>
        <form class="form-container" action="duel" method="post">
            <input class="duel" type="submit" value="Дуэли">
        </form>
        <form class="form-container" action="logout" method="post">
            <input class="logout" type="submit" value="Выход">
        </form>
    </div>
</div>

<div class="bottom-outer-div">
    <div class="inner-div">
        <%
            long end = System.currentTimeMillis();
            long elapsed = end - start;
        %>
        <%="page: " + elapsed + " ms, db: " + dbReqs + " req (" + dbTime + " ms)"%>
    </div>
</div>

</body>
</html>