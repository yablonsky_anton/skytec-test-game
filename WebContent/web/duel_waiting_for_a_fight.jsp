<%
    long start;
    HttpSession session = request.getSession(false);

    long dbTime = (long) session.getAttribute("DBWaitTime");
    int dbReqs = (int) session.getAttribute("DBWaitReqs");

    if (request.getMethod().equalsIgnoreCase("POST")) {
        start = (long) request.getAttribute("PageStartTime");

    } else {
        start = System.currentTimeMillis();
    }%>
<%@ page session="false" %>
<%@ page import="ru.skytecgames.test_game.player.Player" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<style>
    .top-outer-div {
        position: absolute;
        width: 100%
    }

    .bottom-outer-div {
        position: absolute;
        bottom: 10px;
        width: 100%
    }

    .inner-div {
        background-color: rgba(242, 242, 242, .3);
        width: 50%;
        margin: 10px auto auto auto;
        padding: 20px
    }

    * {
        font-family: Helvetica, serif;
        border-radius: 5px;
        box-sizing: border-box
    }

    body {
        background-image: url(../images/background.svg);
        background-color: #ccc;
        background-repeat: no-repeat;
        background-size: cover
    }

    h4 {
        font-weight: normal;
    }


</style>
<head>
    <title>Duel Page</title>
</head>

<body>
<div class="top-outer-div">
    <div class="inner-div">
        <%
            Player enemy = (Player) session.getAttribute("Enemy"); %>
        <h3>Приготовься к бою!</h3>
        <%if ((int) session.getAttribute("Counter") > 0) {%>
        <h4><%="Подобран противник - " + enemy.getUsername()%>
        </h4>
        <h4><%="Бой начнется через " + session.getAttribute("Counter") + " сек"%>
        </h4>
        <%
                session.setAttribute("Counter", ((int) session.getAttribute("Counter") - 1));
                response.setHeader("Refresh", "1");
            } else {
                session.setAttribute("StateOfDuel", "Duel");
                session.removeAttribute("DBtime");
                session.removeAttribute("DBreqs");
                request.getRequestDispatcher("/duel").forward(request, response);
            } %>
    </div>
</div>

<div class="bottom-outer-div">
    <div class="inner-div">
        <%
            long end = System.currentTimeMillis();
            long elapsed = end - start;
        %>
        <%="page: " + elapsed + " ms, db: " + dbReqs + " req (" + dbTime + " ms)"%>
    </div>
</div>
</body>
</html>
