<% long start;
    long dbTime = 0L;
    int dbReqs = 0;
    if (request.getMethod().equalsIgnoreCase("POST")) {
        start = (long) request.getAttribute("PageStartTime");
        dbTime = (long) request.getAttribute("DBtime");
        dbReqs = (int) request.getAttribute("DBreqs");
    } else {
        start = System.currentTimeMillis();
    }%>

<%@ page session="false" %>
<%@ page language="java" contentType="text/html; charset=UTF-8"
         pageEncoding="UTF-8" %>
<!DOCTYPE html>
<html>
<style>
    .top-outer-div {
        position: absolute;
        width: 100%
    }

    .bottom-outer-div {
        position: absolute;
        bottom: 10px;
        width: 100%
    }

    .inner-div {
        background-color: rgba(242, 242, 242, .3);
        width: 50%;
        margin: 10px auto auto auto;
        padding: 20px
    }

    * {
        font-family: Helvetica, serif;
        border-radius: 5px;
        box-sizing: border-box
    }

    body {
        background-image: url(../images/background.svg);
        background-color: #ccc;
        background-repeat: no-repeat;
        background-size: cover
    }

    input[type=password], input[type=text], select {
        width: 100%;
        padding: 12px 20px;
        margin: 8px 0;
        display: inline-block;
        border: 1px solid #ccc;
        font-size: 9pt
    }

    input[type=submit] {
        width: 100%;
        background-color: #af9242;
        color: #fff;
        padding: 14px 20px;
        margin: 8px 0;
        border: 0;
        cursor: pointer;
        font-size: 10pt
    }

    input[type=submit]:hover {
        background-color: #806340
    }
</style>
<head>
    <meta charset="UTF-8">
    <title>Login Page</title>
</head>
<body>

<div class="top-outer-div">
    <div class="inner-div">
        <h3>Введите Ваше имя и пароль</h3>
        <form action="login" method="post">
            <input type="text" name="username" maxlength="20" placeholder="Ваше имя">
            <input type="password" name="password" maxlength="20" placeholder="Ваш пароль">
            <input type="submit" value="Войти">
            <%if (request.getAttribute("error") != null) {%>
            <p style="color:red"><%=request.getAttribute("error")%>
            </p>
            <%}%>
        </form>
    </div>
</div>

<div class="bottom-outer-div">
    <div class="inner-div">
        <%
            long end = System.currentTimeMillis();
            long elapsed = end - start;
        %>
        <%="page: " + elapsed + " ms, db: " + dbReqs + " req (" + dbTime + " ms)"%>
    </div>
</div>
</body>
</html>