<% long start;
    long dbTime = 0L;
    int dbReqs = 0;
    if (request.getMethod().equalsIgnoreCase("POST")) {
        start = (long) request.getAttribute("PageStartTime");
        dbTime = (long) request.getAttribute("DBtime");
        dbReqs = (int) request.getAttribute("DBreqs");
    } else {
        start = System.currentTimeMillis();
    }
    HttpSession session = request.getSession(false);
    String winner = (String) session.getAttribute("Winner");
    Player player = (Player) session.getAttribute("Player");
%>

<%@ page session="false" %>
<%@ page import="ru.skytecgames.test_game.log.DuelLog" %>
<%@ page import="ru.skytecgames.test_game.player.Player" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<style>

    .top-outer-div {
        position: absolute;
        width: 100%
    }

    .bottom-outer-div {
        position: absolute;
        bottom: 10px;
        width: 100%
    }

    .inner-div {
        background-color: rgba(242, 242, 242, .3);
        width: 50%;
        margin: 10px auto auto auto;
        padding: 20px
    }

    * {
        font-family: Helvetica, serif;
        border-radius: 5px;
        box-sizing: border-box
    }

    body {
        background-image: url(../images/background.svg);
        background-color: #ccc;
        background-repeat: no-repeat;
        background-size: cover
    }


    input[type=submit] {
        width: 100%;
        background-color: #af9242;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        cursor: pointer;
        font-size: 10pt;
    }

    input[type=submit]:hover {
        background-color: #806340;
    }


    h4 {
        font-weight: normal;
    }
</style>
<head>
    <title>Duel Page</title>
</head>
<body>
<div class="top-outer-div">
    <div class="inner-div">
        <%if (winner.equals("Player")) {%>
        <h3>Победа!</h3>
        <h4><%="Поздравляем! Теперь твой рейтинг " + player.getRating() + ", твое здоровье " + player.getHealth() + ", твой урон " + player.getDamage()%>
        </h4>
        <form action="duel" method="post">
            <input type="submit" value="Вернуться назад">
        </form>
        <%} else {%>
        <h3>Поражение!</h3>
        <h4><%="Ты проиграл! Теперь твой рейтинг " + player.getRating() + ", твое здоровье " + player.getHealth() + ", твой урон " + player.getDamage()%>
        </h4>
        <form action="duel" method="post">
            <input type="submit" value="Вернуться назад">
        </form>
        <%}%>
    </div>
</div>


<div class="bottom-outer-div">
    <%
        DuelLog duelLog = (DuelLog) session.getAttribute("DuelLog");
        if (!duelLog.getLog().isEmpty()) {%>
    <div class="inner-div">
        <%=duelLog.getLog()%>
        <%if (request.getAttribute("Ending") != null) {%>
        <%=request.getAttribute("Ending")%>
        <%}%>
    </div>
    <%}%>

    <div class="inner-div">
        <%
            long end = System.currentTimeMillis();
            long elapsed = end - start;
        %>
        <%="page: " + elapsed + " ms, db: " + dbReqs + " req (" + dbTime + " ms)"%>
    </div>
</div>
</body>
</html>
