<% long start;
    long dbTime = 0L;
    int dbReqs = 0;
    if (request.getMethod().equalsIgnoreCase("POST")) {
        start = (long) request.getAttribute("PageStartTime");
        dbTime = (long) request.getAttribute("DBtime");
        dbReqs = (int) request.getAttribute("DBreqs");
    } else {
        start = System.currentTimeMillis();
    }
    HttpSession session = request.getSession(false);
    Player player = (Player) session.getAttribute("Player");
    Player enemy = (Player) session.getAttribute("Enemy");
    Player playerBeforeDuel = (Player) session.getAttribute("PlayerBeforeDuel");
    Player enemyBeforeDuel = (Player) session.getAttribute("EnemyBeforeDuel");
    double playerMaxHealth = playerBeforeDuel.getHealth();
    double enemyMaxHealth = enemyBeforeDuel.getHealth();
    double userHealthInPercent = player.getHealth() / playerMaxHealth * 100;
    double enemyHealthInPercent = enemy.getHealth() / enemyMaxHealth * 100;
%>
<%@ page session="false" %>
<%@ page import="ru.skytecgames.test_game.log.DuelLog" %>
<%@ page import="ru.skytecgames.test_game.player.Player" %>
<%@ page import="static jdk.nashorn.internal.runtime.regexp.joni.Config.log" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<style>

    .top-outer-div {
        position: absolute;
        width: 100%
    }

    .bottom-outer-div {
        position: absolute;
        bottom: 10px;
        width: 100%
    }

    .inner-div {
        background-color: rgba(242, 242, 242, .3);
        width: 50%;
        margin: 10px auto auto auto;
        padding: 20px
    }

    * {
        font-family: Helvetica, serif;
        border-radius: 5px;
        box-sizing: border-box
    }

    body {
        background-image: url(../images/background.svg);
        background-color: #ccc;
        background-repeat: no-repeat;
        background-size: cover
    }


    .player-container {
        float: left;
        width: 50%;
        padding: 5px;
    }

    .inner-div::after {
        content: "";
        clear: both;
        display: table;
    }


    .progress {
        height: 2em;
        width: 100%;
        background-color: #c9c9c9;
        position: relative;
    }

    .progress:before {
        content: attr(data-label);
        font-size: 1em;
        position: absolute;
        text-align: center;
        top: 0.5em;
        left: 0;
        right: 0;
    }

    .progress .player {
        border-radius: 5px;

    <%
    if (userHealthInPercent<20){
    %> background-color: #691710;
    <%
    }else{
    %> background-color: #016901;
    <%
    }
    %> display: inline-block;
        height: 100%;
    }

    .progress .enemy {
        border-radius: 5px;

    <%
     if (enemyHealthInPercent<20){
         %> background-color: #691710;
    <%}else{%> background-color: #016901;
    <%}
    %> display: inline-block;
        height: 100%;
    }


    input[type=submit] {
        width: 100%;
        background-color: #b23f1b;
        color: white;
        padding: 14px 20px;
        margin: 8px 0;
        border: none;
        border-radius: 4px;
        cursor: pointer;
        font-size: 10pt;
    }

    input[type=submit]:hover {
        background-color: #802b17;
    }

</style>
<head>
    <title>Duel Page</title>
</head>
<body>
<div class="top-outer-div">
    <div class="inner-div">
        <div class="player-container">
            <h4><%= "Мой урон: " + player.getDamage()%>
            </h4>
            <div class="progress" data-label="<%=player.getHealth()%>">
                <span class="player" style="width:<%=userHealthInPercent%>%;"></span>
            </div>
        </div>

        <div class="player-container">
            <h4><%= "Урон " + enemy.getUsername() + ": " + enemy.getDamage()%>
            </h4>
            <div class="progress" data-label="<%=enemy.getHealth()%>">
                <span class="enemy" style="width:<%=enemyHealthInPercent%>%;"></span>
            </div>
        </div>

        <form action="duel" method="post">
            <input type="submit" name="Attack" value="Атаковать"/>
        </form>
    </div>
</div>


<div class="bottom-outer-div">

    <%
        DuelLog duelLog = (DuelLog) session.getAttribute("DuelLog");
        if (!duelLog.getLog().isEmpty()) {%>
    <div class="inner-div">
        <%=duelLog.getLog()%>
    </div>
    <%}%>

    <div class="inner-div">
        <%
            long end = System.currentTimeMillis();
            long elapsed = end - start;
        %>
        <%="page: " + elapsed + " ms, db: " + dbReqs + " req (" + dbTime + " ms)"%>
    </div>
</div>

</body>
</html>
