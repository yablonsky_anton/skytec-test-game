<% long start;
    long dbTime = 0L;
    int dbReqs = 0;
    if (request.getMethod().equalsIgnoreCase("POST")) {
        start = (long) request.getAttribute("PageStartTime");
        dbTime = (long) request.getAttribute("DBtime");
        dbReqs = (int) request.getAttribute("DBreqs");
    } else {
        start = System.currentTimeMillis();
    }
    HttpSession session = request.getSession(false);
    Player player = (Player) session.getAttribute("Player");
%>
<%@ page session="false" %>
<%@ page import="ru.skytecgames.test_game.player.Player" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<style>
    .top-outer-div {
        position: absolute;
        width: 100%
    }

    .bottom-outer-div {
        position: absolute;
        bottom: 10px;
        width: 100%
    }

    .inner-div {
        background-color: rgba(242, 242, 242, .3);
        width: 50%;
        margin: 10px auto auto auto;
        padding: 20px
    }

    * {
        font-family: Helvetica, serif;
        border-radius: 5px;
        box-sizing: border-box
    }

    body {
        background-image: url(../images/background.svg);
        background-color: #ccc;
        background-repeat: no-repeat;
        background-size: cover;
    }

    input[type=submit] {
        width: 100%;
        background-color: #af9242;
        color: #fff;
        padding: 14px 20px;
        margin: 8px 0;
        border: 0;
        cursor: pointer;
        font-size: 10pt
    }

    input[type=submit]:hover {
        background-color: #806340
    }

    h4 {
        font-weight: normal;
    }

</style>
<head>
    <title>Duel Page</title>
</head>
<body>
<div class="top-outer-div">
    <div class="inner-div">
        <h3>Ваш рейтинг</h3>
        <h4><%=player.getUsername()%>, Ваш рейтинг равняется <%=player.getRating()%>
        </h4>
        <form action="duel" method="post">
            <input type="submit" name="BeginDuel" value="Начать дуэль"/>
        </form>
    </div>
</div>

<div class="bottom-outer-div">
    <div class="inner-div">
        <%
            long end = System.currentTimeMillis();
            long elapsed = end - start;
        %>
        <%="page: " + elapsed + " ms, db: " + dbReqs + " req (" + dbTime + " ms)"%>
    </div>
</div>
</body>
</html>