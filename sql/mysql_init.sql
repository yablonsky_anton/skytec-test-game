# создание пользователя БД для игры
create user 'game'@'localhost' identified by 'game';

#создание БД
CREATE DATABASE IF NOT EXISTS PlayerDB;
USE PlayerDB;
DROP TABLE IF EXISTS `Players`;
CREATE TABLE `Players`
(
  #основные хар-ки игрока и пароль
  `username`        varchar(20) NOT NULL DEFAULT '',
  `password`        varchar(20) NOT NULL DEFAULT '',
  `health`          int(20)     NOT NULL DEFAULT '100',
  `damage`          int(20)     NOT NULL DEFAULT '10',
  `rating`          int(20)     NOT NULL DEFAULT '0',
  # статус "готов к дуэли"
  `isReadyForADuel` boolean     NOT NULL DEFAULT '0',
  #обозначение выбранного противника, чтобы другие игроки при поиске не смогли выбрать этого игрока для дуэли
  `assignedEnemy`   varchar(20),
  PRIMARY KEY (`username`)
)
  ENGINE = InnoDB
  DEFAULT CHARSET = utf8;

#присвоение пользователю всех привилегий для БД
GRANT ALL PRIVILEGES ON PlayerDB.* TO 'game'@'localhost';