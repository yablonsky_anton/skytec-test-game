package ru.skytecgames.test_game.database;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

//Класс для соединения с БД
public class DBConnectionManager {

    private Connection connection;

    public DBConnectionManager(String dbURL, String user, String pwd) throws ClassNotFoundException, SQLException {
        Class.forName("com.mysql.cj.jdbc.Driver");
        this.connection = DriverManager.getConnection(dbURL, user, pwd);
    }

    public Connection getConnection() {
        return this.connection;
    }
}
