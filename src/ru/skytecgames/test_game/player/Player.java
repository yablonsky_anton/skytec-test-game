package ru.skytecgames.test_game.player;

//Класс описывающий игрока
public class Player implements Cloneable {

    private String username;
    private int health;
    private int damage;
    private int rating;

    public Player(String username, int health, int damage, int rating) {
        this.username = username;
        this.health = health;
        this.damage = damage;
        this.rating = rating;
    }

    public Player(String username) {
        this.username = username;
        this.health = 100;
        this.damage = 10;
        this.rating = 0;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public int getHealth() {
        return health;
    }

    public void setHealth(int health) {
        this.health = health;
    }

    public int getDamage() {
        return damage;
    }

    public void setDamage(int damage) {
        this.damage = damage;
    }

    public int getRating() {
        return rating;
    }

    public void setRating(int rating) {
        this.rating = rating;
    }

    @Override
    public String toString() {
        return "Player{" +
                "username='" + username + '\'' +
                ", health=" + health +
                ", damage=" + damage +
                ", rating=" + rating +
                '}';
    }
}
