package ru.skytecgames.test_game.player;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;


//Класс для CRU операций с БД
public class PlayerService {

    private Connection connection;

    private int totalReqs = 0;
    private long totalTime = 0L;

    public PlayerService(Connection connection) {
        this.connection = connection;
    }

    //Поиск в БД пользователя с введенными именем и паролем на странице авторизации
    //Если такой пользователь найден, то вернуть этого пользователя ; если неверный пароль, то вернуть null;
    // если не найден, то создать такого пользователя и добавить в БД
    public Player findPlayerWithPassword(String username, String password) throws SQLException {
        String sql = "SELECT username, password, health, damage, rating FROM Players WHERE username=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, username);
            try (ResultSet rs = executeQueryAndMeasure(ps)) {
                if (rs != null && rs.next()) {
                    if (rs.getString("password").equals(password)) {
                        return new Player(rs.getString("username"), rs.getInt("health"),
                                rs.getInt("damage"), rs.getInt("rating"));
                    } else {
                        return null;
                    }
                } else {
                    Player player = new Player(username);
                    insertPlayerWithPassword(player, password);
                    return player;
                }
            }
        }
    }

    //Создание нового пользователя
    private void insertPlayerWithPassword(Player player, String password) throws SQLException {
        String sql = "INSERT INTO Players(username, password, health, damage, rating) VALUES (?, ?, ?, ?, ?)";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, player.getUsername());
            ps.setString(2, password);
            ps.setInt(3, player.getHealth());
            ps.setInt(4, player.getDamage());
            ps.setInt(5, player.getRating());
            executeAndMeasure(ps);
        }

    }


    //Переключение статуса игрока "Готов к дуэли" на нужный
    public void setPlayerReadinessForADuel(Player player, boolean isReadyForADuel) throws SQLException {
        String sql = "UPDATE Players SET isReadyForADuel=? WHERE username=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setBoolean(1, isReadyForADuel);
            ps.setString(2, player.getUsername());
            executeUpdateAndMeasure(ps);
        }

    }

    //Найти противника, который выбрал игрока для боя
    public Player findEnemyWhichAssignedPlayer(Player player) throws SQLException {
        String sql = "SELECT username, health, damage, rating FROM Players WHERE assignedEnemy=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, player.getUsername());
            try (ResultSet rs = executeQueryAndMeasure(ps)) {
                if (rs != null && rs.next()) {
                    Player enemy = new Player(rs.getString("username"), rs.getInt("health"),
                            rs.getInt("damage"), rs.getInt("rating"));
                    return enemy;
                } else {
                    return null;
                }
            }
        }
    }

    //Найди противника, готового к бою и никогого еще не выбравшего
    public Player findEnemyReadyForADuel(Player player, long quitSearch) throws SQLException {
        String sql = "SELECT username, health, damage, rating "
                + "FROM Players "
                + "WHERE isReadyForADuel=? AND username!=? AND assignedEnemy IS NULL "
                + "ORDER BY assignedEnemy DESC, RAND()"
                + "LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setBoolean(1, true);
            ps.setString(2, player.getUsername());
            try (ResultSet rs = executeQueryAndMeasure(ps)) {
                if (rs != null && rs.next()) {
                    //после выбора противника присвоить себе его в противники
                    assignEnemyToPlayer(player, rs.getString("username"));
//                    проверять назначил ли противник себе игрока в ответ
                    while (System.currentTimeMillis() < quitSearch) {
                        Player enemy = findEnemyWhichAssignedPlayer(player);
                        if (enemy != null) {
                            return enemy;
                        }
                    }
                    return null;
                } else {
                    return null;
                }
            }
        }
    }


    //Выбрать противника для игрока и присвоить его игроку (нужно, чтобы никто не мог выбрать игрока для боя)
    public void assignEnemyToPlayer(Player player, String enemyName) throws SQLException {
        String sql = "UPDATE Players SET assignedEnemy=? WHERE username=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, enemyName);
            ps.setString(2, player.getUsername());
            executeUpdateAndMeasure(ps);
        }
    }

    //Вернуть игрока с актуальным здоровьем
    public Player getPlayerWithActualHealth(Player player) throws SQLException {
        String sql = "SELECT health FROM Players WHERE username=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setString(1, player.getUsername());
            try (ResultSet rs = executeQueryAndMeasure(ps)) {
                rs.next();
                player.setHealth(rs.getInt("health"));
                return player;
            }
        }
    }


    //Обновить игрока в БД после дуэли
    public void updatePlayerAfterDuel(Player player) throws SQLException {
        String sql = "UPDATE Players SET health=?, rating=?, damage=?, isReadyForADuel=0, assignedEnemy=NULL " +
                "WHERE username=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, player.getHealth());
            ps.setInt(2, player.getRating());
            ps.setInt(3, player.getDamage());
            ps.setString(4, player.getUsername());
            executeUpdateAndMeasure(ps);
        }

    }

    //Обновлять здоровье противника во время битвы
    public void updateHealth(Player player) throws SQLException {
        String sql = "UPDATE Players SET health=? WHERE username=? LIMIT 1";
        try (PreparedStatement ps = connection.prepareStatement(sql)) {
            ps.setInt(1, player.getHealth());
            ps.setString(2, player.getUsername());
            executeUpdateAndMeasure(ps);
        }

    }


    //Замерить время выполнения команды executeUpdate и количество вызовов
    private void executeUpdateAndMeasure(PreparedStatement ps) throws SQLException {
        long start = System.currentTimeMillis();
        ps.executeUpdate();
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        totalReqs++;
        totalTime += elapsed;
    }

    //Замерить время выполнения команды execute и количество вызовов
    private void executeAndMeasure(PreparedStatement ps) throws SQLException {
        long start = System.currentTimeMillis();
        ps.execute();
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        totalReqs++;
        totalTime += elapsed;
    }

    //Замерить время выполнения команды executeQuery и количество вызовов
    private ResultSet executeQueryAndMeasure(PreparedStatement ps) throws SQLException {
        long start = System.currentTimeMillis();
        ResultSet rs = ps.executeQuery();
        long end = System.currentTimeMillis();
        long elapsed = end - start;
        totalReqs++;
        totalTime += elapsed;
        return rs;
    }

    public int getTotalReqs() {
        return totalReqs;
    }

    public long getTotalTime() {
        return totalTime;
    }
}
