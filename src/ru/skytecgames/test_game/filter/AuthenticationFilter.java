package ru.skytecgames.test_game.filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//Класс для запрета ходить на все ссылки кроме login неавторизованным пользователям
@WebFilter("/*")
public class AuthenticationFilter implements Filter {

    public AuthenticationFilter() {
    }

    public void destroy() {
    }

    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {

        HttpServletRequest req = (HttpServletRequest) request;
        HttpServletResponse res = (HttpServletResponse) response;

        String uri = req.getRequestURI();
        HttpSession session = req.getSession(false);

        if (session == null && !(uri.equalsIgnoreCase("/login")) && !(uri.endsWith("svg"))) {
            res.sendRedirect("/login");
        } else if (session != null && (uri.equalsIgnoreCase("/login")) || (uri.endsWith("/"))) {
            res.sendRedirect("/menu");
        } else {
            chain.doFilter(request, response);
        }

    }

    public void init(FilterConfig fConfig) throws ServletException {
    }

}
