package ru.skytecgames.test_game.listeners;

import ru.skytecgames.test_game.log.DuelLog;
import ru.skytecgames.test_game.player.Player;

import javax.servlet.ServletContextListener;
import javax.servlet.annotation.WebListener;
import javax.servlet.http.*;
import java.util.HashMap;

//Класс для отслеживания добавления атрибутов сессии. Нужен для логирования боя
@WebListener
public class SessionsListener implements ServletContextListener,
        HttpSessionListener, HttpSessionAttributeListener {

    private HashMap<String, HttpSession> hashMap = new HashMap<>();

    // Public constructor is required by servlet spec
    public SessionsListener() {
    }


    public void sessionDestroyed(HttpSessionEvent se) {
        HttpSession session = se.getSession();
        String playerName = ((Player) session.getAttribute("Player")).getUsername();
        hashMap.remove(playerName);
    }

    synchronized public void attributeAdded(HttpSessionBindingEvent sbe) {
        String attributeName = sbe.getName();
        HttpSession session = sbe.getSession();
        //если добавлен аттрибут игрок, то сессия игрока помещается в коллекцию
        if (attributeName == "Player") {
            String playerName = ((Player) session.getAttribute("Player")).getUsername();
            hashMap.put(playerName, session);
            //если добавлен аттрибут противник, то сессия противника извлекается из коллеции
        } else if (attributeName == "Enemy") {
            String enemyName = ((Player) session.getAttribute("Enemy")).getUsername();
            HttpSession enemySession = hashMap.get(enemyName);
            //если аттрибут сессии DuelLog не сущ-т, создать объект для логирования боя и уст-ть его аттрибутом сессии.
            if (enemySession.getAttribute("DuelLog") == null) {
                DuelLog duelLog = new DuelLog();
                session.setAttribute("DuelLog", duelLog);
                //если аттрибут сущ-т, то достать объект для логирования и установить его аттрибутом сессии. Таким образом
                //достигается синхронизация объекта логирования между двумя играющими друго с другом игроками
            } else {
                DuelLog duelLog = (DuelLog) enemySession.getAttribute("DuelLog");
                session.setAttribute("DuelLog", duelLog);
            }

        }

    }
}
