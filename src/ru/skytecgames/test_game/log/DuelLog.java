package ru.skytecgames.test_game.log;

//Объект хранящий лог боя
public class DuelLog {

    private String log = "";

    public String getLog() {
        return log;
    }

    public void setLog(String log) {
        this.log += log;
    }

}
