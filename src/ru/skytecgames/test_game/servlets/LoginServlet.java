package ru.skytecgames.test_game.servlets;

import ru.skytecgames.test_game.player.Player;
import ru.skytecgames.test_game.player.PlayerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

//Сервлет логина
@WebServlet("/login")
public class LoginServlet extends HttpServlet {

    public LoginServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.getRequestDispatcher("/web/login.jsp").forward(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long start = System.currentTimeMillis();

        //Установка атрибута для замера времени генерации страницы
        request.setAttribute("PageStartTime", start);

        //Берем логин и пароль из полей
        String username = request.getParameter("username");
        String password = request.getParameter("password");


        Connection con = (Connection) getServletContext().getAttribute("database");
        PlayerService playerService = new PlayerService(con);

        String errorMsg = null;

        //Проверка полей имени и пароля. Если они пустые, то вывести ошибку.
        if (username == null || username.isEmpty() || password == null || password.isEmpty()) {
            errorMsg = "Поля имени и пароля не должны быть пустыми!";
            request.setAttribute("error", errorMsg);
            setDBMeasureAttributes(request, playerService);
            doGet(request, response);
        } else {
            try {
                //Если непустые, то выполнить поиск таких сочетаний имени и пароля в БД.
                //Если пароль совпадет, то метод findPlayerWithPassword вернет объект игрока с хар-ми из БД
                //Если не совпадет, то вернет null
                //Если такого имени в БД нет, то будет созданы записи в БД и метод вернет объект игрока.
                Player player = playerService.findPlayerWithPassword(username, password);
                //Если null, то вывести ошибку
                if (player == null) {
                    errorMsg = "Неправильно введен пароль!";
                    request.setAttribute("error", errorMsg);
                    setDBMeasureAttributes(request, playerService);
                    doGet(request, response);
                }
                //Если не null, то авторизовать игрока и перейти на след-ю страницу
                else {
                    HttpSession session = request.getSession();
                    session.setAttribute("Player", player);
                    session.setAttribute("DBMenuTime", playerService.getTotalTime());
                    session.setAttribute("DBMenuReqs", playerService.getTotalReqs());
                    response.sendRedirect("/menu");
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    //Установка атрибутов для замера техничской информации о соединении с БД
    private void setDBMeasureAttributes(HttpServletRequest request, PlayerService playerService) {
        request.setAttribute("DBtime", playerService.getTotalTime());
        request.setAttribute("DBreqs", playerService.getTotalReqs());
    }


}
