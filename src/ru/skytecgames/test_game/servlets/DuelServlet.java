package ru.skytecgames.test_game.servlets;

import ru.skytecgames.test_game.log.DuelLog;
import ru.skytecgames.test_game.player.Player;
import ru.skytecgames.test_game.player.PlayerService;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.sql.Connection;
import java.sql.SQLException;

//Сервлет дуэли
@WebServlet("/duel")
public class DuelServlet extends HttpServlet {

    public DuelServlet() {
        super();
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession(false);
        //Проверка состояния дуэли. null - дуэль не началась. Wait - ожидание боя. Duel - бой. Finish - показ информации после боя.
        if (session.getAttribute("StateOfDuel") == null) {
            request.getRequestDispatcher("/web/duel_rating.jsp").forward(request, response);
        } else if (session.getAttribute("StateOfDuel").equals("Wait")) {
            request.getRequestDispatcher("/web/duel_waiting_for_a_fight.jsp").forward(request, response);
        } else if (session.getAttribute("StateOfDuel").equals("Duel")) {
            request.getRequestDispatcher("/web/duel_fight.jsp").forward(request, response);
        } else if (session.getAttribute("StateOfDuel").equals("Finish")) {
            request.getRequestDispatcher("/web/duel_finish.jsp").forward(request, response);
        }

    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long start = System.currentTimeMillis();
        request.setAttribute("PageStartTime", start);
        HttpSession session = request.getSession(false);
        Connection con = (Connection) getServletContext().getAttribute("database");
        PlayerService playerService = new PlayerService(con);
        Player player = (Player) session.getAttribute("Player");

        //Если нажата кнопка "Дуэли" то начать подбор противника, если нажата "Атаковать" то выполнять атаку, если ни одна не нажата вывести страницу
        if (request.getParameter("Attack") != null) {
            try {
                attack(player, playerService, session, request, response);
            } catch (SQLException e) {
                e.printStackTrace();
            }
        } else if (request.getParameter("BeginDuel") != null) {
            try {
                searchForEnemy(player, playerService, session, request, response);
            } catch (SQLException | CloneNotSupportedException e) {
                e.printStackTrace();
            }
        } else {
            setDBMeasureAttributes(request, playerService);
            doGet(request, response);
        }

    }

    //Задание атрибутов сессии для показа технической информации о соединяниях с БД пока идет отсчет таймера
    private void setDBMeasureAttributesForWait(HttpSession session, PlayerService playerService) {
        session.setAttribute("DBWaitTime", playerService.getTotalTime());
        session.setAttribute("DBWaitReqs", playerService.getTotalReqs());
    }

    //Задание атрибутов запроса для показа технической информации о соединяниях с БД в остальных случаях
    private void setDBMeasureAttributes(HttpServletRequest request, PlayerService playerService) {
        request.setAttribute("DBtime", playerService.getTotalTime());
        request.setAttribute("DBreqs", playerService.getTotalReqs());
    }

    //поиск противника в течение 15 секунд. Сперва проверяется выбрал ли кто-то игрока для боя. Если да, то биться с ним.
    //Если нет, то найти противника готового к бою и никого еще не выбравшего. Когда такой противник будет найден,
    // выбрать его для боя и проверять поле в БД assignedEnemy этого противника, чтобы убедиться, что он подтверждает
    // предложение о бое. Если истекает 15 секунд и никто не выбран, то сбросить все установленные в ходе поиска записи
    // в БД и аттрибуты
    private void searchForEnemy(Player player, PlayerService playerService, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws SQLException, CloneNotSupportedException, ServletException, IOException {
        //сообщает об успешном нахождении противника
        boolean success = false;
        //установка статуса "готов к дуэли"
        playerService.setPlayerReadinessForADuel(player, true);
        //Задание времени поиска противника (15000 = 15 сек) и поиск противника в течение этого времени
        long quitSearch = System.currentTimeMillis() + 15000;
        while (System.currentTimeMillis() < quitSearch) {
            //Проверка, выбрал ли кто-то игрока для боя. Если метод findEnemyWhichAssignedPlayer вернул null, то никто
            // не выбрал игрока.
            //Если не null, то вернут объект противника.
            Player enemy = playerService.findEnemyWhichAssignedPlayer(player);
            //Если метод вернул противника, то установить необходимые атрибуты сессии, выбрать противника для
            // битвы и начать битву.
            if (enemy != null) {
                setAttributesBeforeDuel(player, enemy, session);
                playerService.assignEnemyToPlayer(player, enemy.getUsername());
                setDBMeasureAttributesForWait(session, playerService);
                doGet(request, response);
                success = true;
                break;
            }
            //В другом случае искать противника, еще никого не выбравшего.
            else {
                enemy = playerService.findEnemyReadyForADuel(player, quitSearch);
                //Если такой противник находится, выбрать его.
                if (enemy != null) {
                    setAttributesBeforeDuel(player, enemy, session);
                    success = true;
                    setDBMeasureAttributesForWait(session, playerService);
                    doGet(request, response);
                    break;
                }
            }
        }
        //Если за 15 сек никто не найден, то сбросить все установленные в ходе поиска записи в БД и аттрибуты
        if (!success) {
            playerService.setPlayerReadinessForADuel(player, false);
            playerService.assignEnemyToPlayer(player, null);
            session.removeAttribute("StateOfDuel");
            setDBMeasureAttributes(request, playerService);
            doGet(request, response);
        }
    }

    //Установка аттрибутов сессии перед боем.
    private void setAttributesBeforeDuel(Player player, Player enemy, HttpSession session) throws CloneNotSupportedException {
        Player playerBeforeDuel = (Player) player.clone();
        Player enemyBeforeDuel = (Player) enemy.clone();
        //аттрибуты ...BeforeDuel нужны, чтобы хранить хар-ки игрока и противника до боя.
        session.setAttribute("PlayerBeforeDuel", playerBeforeDuel);
        session.setAttribute("EnemyBeforeDuel", enemyBeforeDuel);
        session.setAttribute("Enemy", enemy);
        //счетчик для ожидания перед боем.
        session.setAttribute("Counter", 30);
        session.setAttribute("StateOfDuel", "Wait");
    }

    //метод вызываемый при нажатии кнопки "Атаковать"
    private void attack(Player player, PlayerService playerService, HttpSession session, HttpServletRequest request, HttpServletResponse response) throws SQLException, ServletException, IOException {
        Player enemy = (Player) session.getAttribute("Enemy");

        //достать из БД актуальное здоровье игрока
        player = playerService.getPlayerWithActualHealth(player);
        //объект для логирования боя
        DuelLog duelLog = (DuelLog) session.getAttribute("DuelLog");

        //если здоровье игрока меньше либо равно нулю, то он проиграл.
        if (player.getHealth() <= 0) {
            player = (Player) session.getAttribute("PlayerBeforeDuel");
            finishDuel(player, playerService, session, request);
            player.setRating(player.getRating() - 1);
            session.setAttribute("Winner", "Enemy");
            request.setAttribute("Ending", enemy.getUsername() + " убил Вас");
            doGet(request, response);
            removeAttributesAfterFinish(session);
        }
        //Атаковать противника. После проверить его здоровье, если оно меньше нуля, то он проиграл. Если нет - продолжить битву.
        else {
            enemy.setHealth(enemy.getHealth() - player.getDamage());
            playerService.updateHealth(enemy);
            duelLog.setLog(player.getUsername() + " ударил " + enemy.getUsername() + " на " + player.getDamage() + " урона<br>");
            if (enemy.getHealth() <= 0) {
                player = (Player) session.getAttribute("PlayerBeforeDuel");
                finishDuel(player, playerService, session, request);
                player.setRating(player.getRating() + 1);
                session.setAttribute("Winner", "Player");
                request.setAttribute("Ending", "Вы убили " + enemy.getUsername());
                doGet(request, response);
                removeAttributesAfterFinish(session);
            } else {
                setDBMeasureAttributes(request, playerService);
                doGet(request, response);
            }

        }
    }

    private void finishDuel(Player player, PlayerService playerService, HttpSession session, HttpServletRequest request) throws SQLException {
        player.setHealth(player.getHealth() + 1);
        player.setDamage(player.getDamage() + 1);
        playerService.updatePlayerAfterDuel(player);
        session.setAttribute("Player", player);
        session.setAttribute("StateOfDuel", "Finish");
        setDBMeasureAttributes(request, playerService);


    }

    //Удаление ненужных атрибутов после боя
    private void removeAttributesAfterFinish(HttpSession session) {
        session.removeAttribute("StateOfDuel");
        session.removeAttribute("PlayerBeforeDuel");
        session.removeAttribute("EnemyBeforeDuel");
        session.removeAttribute("Enemy");
        session.removeAttribute("Winner");
        session.removeAttribute("DuelLog");
    }


}
