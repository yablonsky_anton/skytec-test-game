package ru.skytecgames.test_game.servlets;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

//Сервлет главного меню
@WebServlet("/menu")
public class MainMenuServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        long start = System.currentTimeMillis();
        HttpSession session = request.getSession(false);
        request.setAttribute("PageStartTime", start);
        request.getRequestDispatcher("/web/menu.jsp").forward(request, response);
        session.setAttribute("DBMenuTime", 0L);
        session.setAttribute("DBMenuReqs", 0);
    }
}
